import os

import numpy as np
import torch
from torch.utils.data import DataLoader, Dataset


class AudioDataset(Dataset):
    def __init__(self, dataFolder) -> None:
        super().__init__()
        features, targets = self._LoadAudioFiles(dataFolder)
        self.features = features
        self.targets = targets

    def __getitem__(self, index):
        return self.features[index], self.targets[index]
    
    def __len__(self):
        return len(self.features)

    def _LoadAudioFiles(self, dataFolder):
        allFeatures = []
        allTargets = []
        for dataFilePath in os.listdir(dataFolder):
            dataFile = os.path.join(dataFolder, dataFilePath)
            dataContent = np.load(dataFile)
            features, labels, boxes = dataContent['features'], dataContent['labels'], dataContent['boxes']
            features = torch.from_numpy(features).float()
            targets = {
                'labels': torch.from_numpy(labels).long() + 1,
                'boxes': torch.from_numpy(boxes).float()
            }
            allFeatures.append(features)
            allTargets.append(targets)
        return allFeatures, allTargets

def GetDataLoaders(dataFolder):
    trainFolder = dataFolder + '/train'
    validFolder = dataFolder + '/valid'
    trainDataset = AudioDataset(trainFolder)
    validDataset = AudioDataset(validFolder)
    return DataLoader(trainDataset, shuffle=True, batch_size=1, num_workers=6), DataLoader(validDataset, batch_size=1, num_workers=6)



# dataFolder = 'audioDetection/feat/train'
# a = AudioDataset(dataFolder)
# a.features
