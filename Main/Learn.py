import os

import mlflow
import pytorch_lightning as pl
from pytorch_lightning.callbacks.early_stopping import EarlyStopping

from .DataLoader import GetDataLoaders
from .TrainingLoop import LitModule

if __name__ == "__main__":

    trainDataLoader, validDataLoader = GetDataLoaders('audioDetection/feat')
    modelParameters = dict(
        rpn_pre_nms_top_n_train=5000,
        rpn_pre_nms_top_n_test=5000,
        rpn_post_nms_top_n_train=5000,
        rpn_post_nms_top_n_test=5000,
        rpn_nms_thresh=0.5,
        rpn_fg_iou_thresh=0.7,
        rpn_bg_iou_thresh=0.3,
        rpn_batch_size_per_image=500,
        rpn_positive_fraction=0.7,
        rpn_score_thresh=0.0,
        # Box parameters
        box_score_thresh=0.01,
        box_nms_thresh=0.5,
        box_detections_per_img=200,
        box_fg_iou_thresh=0.7,
        box_bg_iou_thresh=0.3,
        box_batch_size_per_image=150,
        box_positive_fraction=0.7,
        bbox_reg_weights=(1e-3, 1., 1e-3, 1.))

    mlflow.set_tracking_uri("file:///C:/Users/mcisse/Documents/projects/Experiments/mlruns")
    mlflow.set_experiment("AudioEventDetection")


    with mlflow.start_run() as run:
        mlflow.log_params(modelParameters)
        device = 0
        plmodel = LitModule(device, modelParameters)
        # early_stop_callback = EarlyStopping(monitor='numPredictions', min_delta=0.00, patience=20, verbose=True, mode='max')
        trainer = pl.Trainer(gpus=[device], max_epochs=100, )
        # callbacks=[early_stop_callback])
        try: 
            trainer.fit(plmodel, train_dataloader=trainDataLoader, val_dataloaders=validDataLoader)
        finally:
            mlflow.pytorch.log_model(plmodel.model, "SavedModels/trainedModel.pth")
