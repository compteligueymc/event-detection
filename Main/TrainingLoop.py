import mlflow
import numpy as np
import pytorch_lightning as pl
import torch
import torch.nn.functional as F
from Core.FasterRCNN import FasterRCNN
from Core.Backbone import createBackBone


# -
# +
class LitModule(pl.LightningModule):

    def __init__(self, device, modelParameters):
        super().__init__()
        numClasses = 7
        numChannels = 40
        outChannels = 64


        # self.device = 'cuda:0'

        faster_rcnn_fe_extractor = createBackBone(numClasses, numChannels, outChannels)
        #TODO check layer of fpn layer / forward of backbone
        self.model = FasterRCNN(backbone= faster_rcnn_fe_extractor, num_classes=numClasses, **modelParameters)

        self.trainParameters = {
            'optimizer': 'AdamW',
            'learningRate': 1e-4,
            'weightDecay': 1e-5
        }

        mlflow.log_params(self.trainParameters)

    def training_step(self, batch, _):
        features, targets = batch
        features = features[0].to(self.device)
        targets = [
            {
                key:value.squeeze(0).to(self.device) for key, value in targets.items()
            }
        ]
        outputs = self.model(features, targets)
        returnedOutputs = {
            'loss': sum([lossValue for lossValue in outputs.values()]),
        }
        returnedOutputs.update({key:value.detach() for key, value in outputs.items()})
        return returnedOutputs

    def training_epoch_end(self, training_step_outputs):
        # loss
        trainLossClassifier = torch.stack([x['loss_classifier'] for x in training_step_outputs]).mean()
        trainLossBoxReg = torch.stack([x['loss_box_reg'] for x in training_step_outputs]).mean()
        trainLossObjectness = torch.stack([x['loss_objectness'] for x in training_step_outputs]).mean()
        trainLossRpnBoxReg = torch.stack([x['loss_rpn_box_reg'] for x in training_step_outputs]).mean()

        self.log('loss_classifier', float(trainLossClassifier), logger=False, prog_bar=True)
        self.log('loss_objectness', float(trainLossObjectness), logger=False, prog_bar=True)

        mlflow.log_metrics(
            {
                'loss_classifier': float(trainLossClassifier),
                'loss_box_reg': float(trainLossBoxReg),
                'loss_objectness': float(trainLossObjectness),
                'loss_rpn_box_reg': float(trainLossRpnBoxReg),
            },
            step=self.current_epoch,
        )

    def validation_step(self, batch, _):
        features, targets = batch
        features = features[0].to(self.device)
        targets = [
            {
                key:value.squeeze(0).to(self.device) for key, value in targets.items()
            }
        ]
        outputs = self.model(features)
        return {
            'numBoxes':len(torch.unique(outputs[0]['labels']))
        }

    def validation_epoch_end(self, validation_step_outputs):
        # metrics
        numPredictions = np.stack([x['numBoxes'] for x in validation_step_outputs]).mean()
        
        self.log('numPredictions', numPredictions, logger=False, prog_bar=True)

        mlflow.log_metrics(
            {
                'numPredictions': float(numPredictions),
            },
            step=self.current_epoch,
        )
        # pass

    def configure_optimizers(self):
        if self.trainParameters["optimizer"] == "Adam":
            optimizer = torch.optim.Adam(params=self.model.parameters(),
                                         lr=self.trainParameters["learningRate"],
                                         weight_decay=self.trainParameters["weightDecay"])
        elif self.trainParameters["optimizer"] == "AdamW":
            optimizer = torch.optim.AdamW(params=self.model.parameters(),
                                          lr=self.trainParameters["learningRate"],
                                          weight_decay=self.trainParameters["weightDecay"])
        else:
            optimizer = torch.optim.Adagrad(params=self.model.parameters(),
                                            lr=self.trainParameters["learningRate"],
                                            weight_decay=self.trainParameters["weightDecay"])
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.5)
        return {
            'optimizer': optimizer, 
            'scheduler': scheduler,
        }

    def get_progress_bar_dict(self):
        tqdmDict = super().get_progress_bar_dict()
        if 'v_num' in tqdmDict:
            del tqdmDict['v_num']
        return tqdmDict
