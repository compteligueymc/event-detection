# Sound event detection using convolutional neural networks

The goal consists recognizing the sound events and their respective temporal start and end time in a recording. Dataloading and processing is done using the code by [sharathadavanne](https://github.com/sharathadavanne). We use a region-based architecture as proposed in **R-CRNN: Region-based Convolutional Recurrent Neural Network for Audio Event Detection.** by Kao et al.
 
![This is an image](EventProposal.png)

So there is an event proposal layer and regression layer. However we did't use recurrent neural networks in feature extraction. Our implementation is an adaption of torchvision's faster-rcnn for 1d data. 

# Current results
Not satisfactory. Below we present ground truth signal events and model predictions
![This is an image](modelOutput1.png)
![This is an image](modelOutput2.png)
